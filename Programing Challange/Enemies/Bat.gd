extends KinematicBody2D

enum{IDLE, WANDER, CHASE}

onready var PlayerDetectionZone =$PlayerDetectionZone

var state=CHASE
var velocity=Vector2.ZERO
export var acceleration=300
export var MaxSpeed=60
export var friction=200

func _physics_process(delta):
	match state:
		IDLE:
			velocity=velocity.move_toward(Vector2.ZERO, 200)
			seek_player()
		WANDER:
			pass
		CHASE:
			var Player=PlayerDetectionZone.Player
			if Player!=null:
				var direction=(Player.global_position - global_position).normalized()
				velocity=velocity.move_toward(direction*MaxSpeed,acceleration*delta)
	velocity=move_and_slide(velocity)
func seek_player():
	if PlayerDetectionZone.can_see_player():
		state=CHASE
